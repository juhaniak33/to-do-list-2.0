<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    public function index(){          
        $user = auth()->user();
        $tasks = Task::where('user_id', $user->id)->get();
        return $tasks;

    }
 
    public function addNewTask(Request $request){ 
        $user = auth()->user();
        $task = new Task();
        $task->name = $request->name;
        $task->user_id = $user->id;
        $task->save();
        return $task;
    }
    
    public function delete(Task $task){
        $task->delete();
    } 
}
