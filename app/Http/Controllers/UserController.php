<?php

namespace App\Http\Controllers;

use App\Http\Middleware\Authenticate;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function login(Request $request){

        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        $user = User::where('email', request('email'))->first();

        if (!$user) {
            return $this->wrongAccess('Email not exit.');
        }

        $access = Hash::check(request('password'), $user->password);

        if (!$access) {
            return $this->wrongAccess('Wrong password.');
        }
        
        if ($access) {
            auth()->login($user);            
            return redirect()->to('/');
        }
    }
    
    public function logout(){
        auth()->logout();
        return true;
    }    
    public function store(Request $request){
        
        $this->validate(request(), [
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);
        
        $user = User::create(request(['email', 'password']));
        auth()->login($user);
        return redirect()->to('/');
    }

    private function wrongAccess($message) {
        return Redirect::back()->withErrors(
            [
                'access' => $message
            ]
        );
    }
}
