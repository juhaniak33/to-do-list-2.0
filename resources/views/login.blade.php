<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"></script>
</head>
<body>
    <div class=" container border border-dark rounded mt-5 w-50 pb-2" >
        <h1>Prihlásenie</h1>
        <form action="/login" method="POST" class="container" >
            @csrf
            <input type="text" placeholder="meno" name="email" class="form-control mb-2">
            <input type="password" placeholder="heslo" name="password" class="form-control mb-2">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <button class="btn btn-primary">Prihlasit</button>
            <a class="btn btn-secondary" href="./register">Registrácia</a>  
        </form>
    </div>
</body>
</html>